Queue data structure is first in first out data structure also known as FIFO. It can be used at many places typically for a producer consumer type of architecture where one component is producing information and other components are consuming them. Queue allows us to implement loosely coupled architecture which has many benefits. I've explained that by using example of new york stock exchange sharing stock price information with yahoo finanace or google finance portals. We will also implement Queue using collections.deque in python

We used lists for quesues as we have issues with dynamic array but we prefer deque from collections for queue deque can be used as a queue or stack

Queue has enqueue(Addiing) and dequeue(poping) methods

wmt_stock_price_queue = []
wmt_stock_price_queue.insert(0,131.10)
wmt_stock_price_queue.insert(0,132.12)
wmt_stock_price_queue.insert(0,135)
wmt_stock_price_queue
[135, 132.12, 131.1]
wmt_stock_price_queue.pop()
131.1
wmt_stock_price_queue
[135, 132.12]
wmt_stock_price_queue.pop()
132.12
wmt_stock_price_queue.pop()
135
wmt_stock_price_queue.pop()
IndexError: pop from empty list



Using collections.deque as a queue
from collections import deque
q = deque()
q.appendleft(5)
q.appendleft(8)
q.appendleft(10)
q
deque([10, 8, 5])

q.pop()
5

q
deque([10, 8])

q.pop()
8

q.pop()
10

q.pop()
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
<ipython-input-21-a75d510a0910> in <module>
----> 1 q.pop()

IndexError: pop from an empty deque