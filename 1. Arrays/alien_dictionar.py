"""
In an alien language, surprisingly, they also use English lowercase letters, but possibly in a different order. The order of the alphabet is
some permutation of lowercase letters.
Given a sequence of words written in the alien language, and the order of the alphabet, return true if and only if the given words are
sorted lexicographically in this alien language.
Example 1:
Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
Output: true
Explanation: As 'h' comes before 'l' in this language, then the sequence is sorted.
Example 2:
Input: words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
Output: false
Explanation: As 'd' comes after 'l' in this language, then words[0] > words[1], hence the sequence is unsorted.
Example 3:
Input: words = ["apple","app"], order = "abcdefghijklmnopqrstuvwxyz"
Output: false
Explanation: The first three characters "app" match, and the second string is shorter (in size.) According to lexicographical rules "apple" > "app", because 'l' > '∅', where '∅' is defined as the blank character which is less than any other character (More info).
"""

"""  
This is an explaination for
    https://leetcode.com/problems/verifying-an-alien-dictionary/discuss/203185/JavaC%2B%2BPython-Mapping-to-Normal-Order
Thanks for this smart solution!
For example,
words = ["hello","leetcode"]
order = "hlabcdefgijkmnopqrstuvwxyz"
Create a dic, key is each word in new order, value is its index, which means its new position in the new order.
dic = {u'a': 2, u'c': 4, u'b': 3, u'e': 6, u'd': 5, u'g': 8, u'f': 7, u'i': 9, u'h': 0, u'k': 11, u'j': 10, u'm': 12, u'l': 1, u'o': 14, u'n': 13, u'q': 16, u'p': 15, u's': 18, u'r': 17, u'u': 20, u't': 19, u'w': 22, u'v': 21, u'y': 24, u'x': 23, u'z': 25}
Transform the list of words into its index in new order.
words = ["hello","leetcode"] -> [[0, 6, 1, 1, 14], [1, 6, 6, 19, 4, 14, 5, 6]]
zip
zip() will zip 2 element one by one,
zip(words, words[1:]) here will combine first element(words[0]) in words with
    words[1], so we can compare current element with next element: 0->1, 1->2

For list comparision, if we want w1 < w2, if len(w1) = len(w2), it will compare each element in w1/w2, if everyone successes, return True.
If len is different. then if len(w1) > len(w2): it will return false since null is less than anything. if len(w1) < len(w2), then as long as everyone in w1 is less than its corresponding cmpoent in w2, it will return True.
This is the same as lexicographicaly sort.
if w1 > w2: since testcase is missing duplicates in the list, so to handle duplicate, we use > instead of >= """

class Solution(object):
    def isAlienSorted(self, words, order):
        """
        :type words: List[str]
        :type order: str
        :rtype: bool
        """
        dic = {}
        new_words = []
        for i, ch in enumerate(order):
            dic[ch] = i
        for w in words:
            new = []
            for c in w:
                new.append(dic[c])
            new_words.append(new)
        for w1, w2 in zip(new_words, new_words[1:]):
            if w1 > w2:
                return False
        return True

"""
items = ("John", "Charles", "Mike", "Sally")
x = zip(items, items[1:])                     # combine "items" (full tuple) with "items" skipping the first entry, till the end
print(tuple(x))                               # to see results of zip, must use use tuple()

(('John', 'Charles'), ('Charles', 'Mike'), ('Mike', 'Sally'))
"""