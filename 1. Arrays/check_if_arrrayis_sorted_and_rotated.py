"""
Given an array nums, return true if the array was originally sorted in non-decreasing order, then rotated some number of positions (including zero). Otherwise, return false.

There may be duplicates in the original array.

Note: An array A rotated by x positions results in an array B of the same length such that A[i] == B[(i+x) % A.length], where % is the modulo operation.

Example 1:

Input: nums = [3,4,5,1,2]
Output: true
Explanation: [1,2,3,4,5] is the original sorted array.
You can rotate the array by x = 3 positions to begin on the the element of value 3: [3,4,5,1,2].
"""

class Solution:
    def check(self, nums: List[int]) -> bool:
        return sum(nums[i] < nums[i-1] for i in range(len(nums))) <= 1

"""
*******
>>> num = [1,2, 3]
>>> a = num[-1]
>>> a
3
>>>

Very clever to use the fact that nums[0] < nums[-1] is also considered in this


"""
"""
If nums is sorted and rotated then there will be at most one consecutive pair i,j such that nums[i] > nums[j] when i>j.So just count how many such pairs exists. If the count is more than 1 then nums is not sorted/rotated.
class Solution(object):
    def check(self, nums):

        if len(nums) == 1: return True
        threshold = 0

        for i in range(len(nums)-1):
            if nums[i] > nums[i+1]: 
                threshold += 1
                
        if threshold and nums[0] < nums[-1]:
            return False
        return True if threshold == 0 or threshold == 1 else False            """
