"""
Given an array arr of integers, check if there exists two integers N and M such that N is the double of M ( i.e. N = 2 * M).
More formally check if there exists two indices i and j such that :
i != j
0 <= i, j < arr.length
arr[i] == 2 * arr[j]
Example 1:
Input: arr = [10,2,5,3]
Output: true
Explanation: N = 10 is the double of M = 5,that is, 10 = 2 * 5.
"""

class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:
        for i, num in enumerate(arr):
            if num % 2 == 0 and (num//2 in arr[i+1:] or num//2 in arr[:i-1]):
                return True

"""
def checkIfExist(self, arr: List[int]) -> bool:
        seen = {}
        for i in range(len(arr)):
            if 2 * arr[i] in seen or arr[i] % 2 == 0 and arr[i] // 2 in seen:
                return True
            seen[arr[i]]=i
        return False
"""

"""
class Solution:

    def checkIfExist(self, arr:List[int]) -> True:
        
        for i in range(len(arr)):
            if arr[i]*2 in arr and arr.index(arr[i]*2 ) != i:
                return True
        
        return False
"""