"""217. Contains Duplicate --> Given an integer array nums, return true if any value appears at least twice in the array,
and return false if every element is distinct.
from typing import List"""
from typing import List

class Solution:
    def containsduplicate(self, nums: List[int]) -> bool:
        from collections import Counter
        dict = Counter(nums)
        for x in dict.values():
            if x > 1:
                return True
        return False
# Easiest is -  return (len(set(nums)) != len(nums))


obj: Solution = Solution()
print(obj.containsduplicate([1, 2, 3, 1]))
print(obj.containsduplicate([1, 2, 3, 4]))
print(obj.containsduplicate([]))


