"""Given an integer array nums and an integer k, return true if there are two distinct indices i and j in the array
such that nums[i] == nums[j] and abs(i - j) <= k.
Example 1:
Input: nums = [1,2,3,1], k = 3
Output: true"""
class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        numsdup = {}
        for i, num in enumerate(nums):
            if num in numsdup and abs(i - numsdup[num]) <= k:
                return True
            else:
                numsdup[num] = i
        return False
