"""
Given an integer array nums and an integer k, return the number of pairs (i, j) where i < j such that |nums[i] - nums[j]| == k.
The value of |x| is defined as:
x if x >= 0.
-x if x < 0.
Example 1:
Input: nums = [1,2,2,1], k = 1
Output: 4
Explanation: The pairs with an absolute difference of 1 are:
- [1,2,2,1]
- [1,2,2,1]
- [1,2,2,1]
- [1,2,2,1]
"""

class Solution:
    def countKDifference(self, nums: List[int], k: int) -> int:
        count = 0
        l = len(nums)
        for i in range(len(nums)):
            for j in range(i+1, len(nums)):
                if abs(nums[i] - nums[j]) == k:
                    count += 1
        return count

#time complexity - o(n*n)
#space complexity - o(n)
"""
class Solution:
    def countKDifference(self, nums: List[int], k: int) -> int:
        return sum(abs(nums[i]-nums[j])==k for i in range(len(nums)) for j in range(i+1,len(nums)))

class Solution:
    def countKDifference(self, nums: List[int], k: int) -> int:
        return sum(abs(a-b)==k for a in nums for b in nums)//2

return [(i - j) == k for i, j in permutations(nums, 2)].count(1)

return [abs(i - j) == k for i, j in combinations(nums, 2)].count(1)

"""

# Hash Map
# Time complexity: O(N)
# Space complexity: O(N)
import collections
class Solution:
    def countKDifference(self, nums: List[int], k: int) -> int:
        res, mydict = 0, collections.defaultdict(int)
        for i in range(len(nums)):
            x1 = nums[i] - k
            x2 = nums[i] + k
            if x1 in mydict:
                res += mydict[x1]
            if x2 in mydict:
                res += mydict[x2]
            mydict[nums[i]] += 1
        return res

"""
class Solution:
    def countKDifference(self, nums: List[int], k: int) -> int:
        seen = defaultdict(int)
        counter = 0
        for num in nums:
            tmp, tmp2 = num - k, num + k
            if tmp in seen:
                counter += seen[tmp]
            if tmp2 in seen:
                counter += seen[tmp2]
            
            seen[num] += 1
        
        return counter
"""