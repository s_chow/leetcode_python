"""
Given two non-negative integers low and high. Return the count of odd numbers between low and high (inclusive).

Example 1:

Input: low = 3, high = 7
Output: 3
Explanation: The odd numbers between 3 and 7 are [3,5,7].
Example 2:

Input: low = 8, high = 10
Output: 1
Explanation: The odd numbers between 8 and 10 are [9].
"""

class Solution:
    def countOdds(self, low: int, high: int) -> int:
        if low % 2 == 0:
            return (high-low+1)//2
        return (high-low)//2 + 1

"""
(high - low + 1) is number of element between high and low where half of them are odd, so (high - low + 1) // 2
and additionally add 1 if high and low are odd for case when there are more odd numbers than even ones (for example 1..3)
"""

odd odd   3  9 --> 3 5 7 9 --- 6  --- 6//2 + 1
odd even  3  10 --> 3 5 7 9 --- 7  --- 7//2 + 1
even even 2  10 --> 3 5 7 9 ---- 8  --- 8+1//2
even odd  2  9 --> 3 5 7 9 --- 7 --- 7+1//2
