"""
You are given an array of unique integers salary where salary[i] is the salary of the ith employee.
Return the average salary of employees excluding the minimum and maximum salary. Answers within 10-5 of the actual answer will be accepted.
Example 1:

Input: salary = [4000,3000,1000,2000]
Output: 2500.00000
Explanation: Minimum salary and maximum salary are 1000 and 4000 respectively.
Average salary excluding minimum and maximum salary is (2000+3000) / 2 = 2500
Example 2:

Input: salary = [1000,2000,3000]
Output: 2000.00000
Explanation: Minimum salary and maximum salary are 1000 and 3000 respectively.
Average salary excluding minimum and maximum salary is (2000) / 1 = 2000
"""

class Solution:
    def average(self, salary: List[int]) -> float:
        s, m, M = 0, float('inf'), float('-inf')
        for num in salary:
            s += num
            m, M = min(m, num), max(M, num)
        return (s - m - M) / (len(salary) - 2)

class Solution:
    def average(self, salary: List[int]) -> float:

        # Solution 1
        salary.remove(max(salary))
        salary.remove(min(salary))
        return sum(salary)/len(salary)

        # Solution 2
        salary.sort()
        salary = salary[1:-1]
        return sum(salary)/len(salary)

        # Solution 3
        salary = sorted(salary)[1:-1]
        return sum(salary)/len(salary)

        # Solution 4
        return (sum(salary) - max(salary) - min(salary)) / (len(salary) - 2)