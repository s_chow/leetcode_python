"""Given an array of integers nums, calculate the pivot index of this array.
The pivot index is the index where the sum of all the numbers strictly to the left of the index is equal to the sum of all the numbers
strictly to the index's right.
If the index is on the left edge of the array, then the left sum is 0 because there are no elements to the left.
 This also applies to the right edge of the array.
Return the leftmost pivot index. If no such index exists, return -1
Input: nums = [1,7,3,6,5,6]
Output: 3
Explanation:
The pivot index is 3.
Left sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11
Right sum = nums[4] + nums[5] = 5 + 6 = 11
Input: nums = [1,2,3]
Output: -1
Explanation:
There is no index that satisfies the conditions in the problem statement.
"""
#Time complexity is O(n)
class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        leftsum = 0
        rightsum = sum(nums)
        for i in range(len(nums)):
            if leftsum == rightsum - nums[i]:
                return i
            leftsum += nums[i]
            rightsum -= nums[i]
        return -1

l 0     1   8   11
r 28    27  20  17

"""
#This solution here as Time complexity is O(n**2)
class Solution:
    def findMiddleIndex(self, nums: List[int]) -> int:
        for i in range(len(nums)):
            if sum(nums[:i]) == sum(nums[i+1:]):
                return i
    
        return -1
"""

class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        leftsum = 0
        rightsum = sum(nums)
        for i in range(len(nums)):
            if leftsum == rightsum - nums[i]:
                return i
            leftsum += nums[i]
            rightsum -= nums[i]
        return -1

"""class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        i = 0
        j = len(nums) - 1
        left  = 0
        right = sum(nums)
        while i<=j:
            left = left + nums[i]
            right = right - nums[j]
            i = i + 1
            j = j - 1 
            if left == right:
                return i
        return -1 """