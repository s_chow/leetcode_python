"""
Given a binary array nums, return the maximum number of consecutive 1's in the array.
Example 1:
Input: nums = [1,1,0,1,1,1]
Output: 3
"""
class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        maxi = 0
        count = 0
        for num in nums:
            if num == 1:
                count = count+1
                maxi = max(maxi, count) #n=(m if m>n else n)
            else:
                count = 0
        return maxi
