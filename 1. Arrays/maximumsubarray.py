"""53. Maximum Subarray
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
A subarray is a contiguous part of an array.
Example 1:
Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6."""

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        largest_til_here = nums[0]
        largest_end_here = nums[0]
        for i in range(1, len(nums)):
            if largest_end_here + nums[i] < nums[i]:
                largest_end_here = nums[i]
            else:
                largest_end_here += nums[i]
            if largest_til_here < largest_end_here:
                largest_til_here = largest_end_here
        return largest_til_here
