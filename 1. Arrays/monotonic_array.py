"""
An array is monotonic if it is either monotone increasing or monotone decreasing.
An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j]. An array nums is monotone decreasing
if for all i <= j, nums[i] >= nums[j].
Given an integer array nums, return true if the given array is monotonic, or false otherwise.
Example 1:
Input: nums = [1,2,2,3]
Output: true
Example 2:
Input: nums = [6,5,4,4]
Output: true
Example 3:
Input: nums = [1,3,2]
Output: false
"""


class Solution:
    def isMonotonic(self, nums: List[int]) -> bool:
        is_increasing = False
        is_decreasing = False
        for i in range(len(nums) - 1):
            if nums[i+1] > nums[i]:
                is_increasing = True
            elif nums[i+1] < nums[i]:
                is_decreasing = True
            if is_increasing and is_decreasing:
                return False
        return True