"""283. Move Zeroes
Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
Note that you must do this in-place without making a copy of the array.
Example 1:
Input: nums = [0,1,0,3,12]
Output: [1,3,12,0,0] """

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        i = 0
        for j in range(len(nums)):
            if nums[j] != 0:
                nums[i], nums[j] = nums[j], nums[i]
                i += 1

"""
In place - We initialize two pointers i = 0, j = 0. Iterate j over range(len(nums)), and if nums[j] != 0,
 we swap nums[i] and nums[j], and increment i by 1. It is easy to see the loop invariant that nums[:i+1] contains all
  nonzero elements in nums[:j+1] with relative order preserved. Hence when j reaches len(nums)-1, nums[:i+1] contains 
  all nonzero elements in nums with relative order preserved.
Time complexity: O(n), space complexity: O(1)
"""

"""
class Solution(object):
	def moveZeroes(self, nums):
		i=0
		n = len(nums)
		while i <n:
			if nums[i]==0:
				nums.pop(i)
				nums.append(0)
				n-=1
			else:
				i+=1
"""