"""
Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.
After doing so, return the array.
Example 1:
Input: arr = [17,18,5,4,6,1]
Output: [18,6,6,6,1,-1]
Explanation:
- index 0 --> the greatest element to the right of index 0 is index 1 (18).
- index 1 --> the greatest element to the right of index 1 is index 4 (6).
- index 2 --> the greatest element to the right of index 2 is index 4 (6).
- index 3 --> the greatest element to the right of index 3 is index 4 (6).
- index 4 --> the greatest element to the right of index 4 is index 5 (1).
- index 5 --> there are no elements to the right of index 5, so we put -1.
"""
class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:
        for i, num in enumerate(arr):
            if i == len(arr)-1:
                arr[i] = -1
            else:
                arr[i] = max(arr[i+1:])
        return arr

"""
Iterate from the back to the start,
We initilize mx = -1, where mx represent the max on the right.
Each round, we set A[i] = mx, where mx is its mas on the right.
Also we update mx = max(mx, A[i]), where A[i] is its original value.
def replaceElements(self, A, mx = -1):
        for i in xrange(len(A) - 1, -1, -1):
            A[i], mx = mx, max(mx, A[i])
        return A
"""