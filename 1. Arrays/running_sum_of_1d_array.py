"""
Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
Return the running sum of nums.

Example 1:

Input: nums = [1,2,3,4]
Output: [1,3,6,10]
Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
Example 2:

Input: nums = [1,1,1,1,1]
Output: [1,2,3,4,5]
Explanation: Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
"""

class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        for i in range(1,len(nums)):
            nums[i] += nums[i-1]
        return nums

"""Space Complexity O(1)
Time complexity O(n)"""

"""---------------------------------------------------------------------------------"""
"""local/global variable issue

class Solution:
    a = 0
    res = []
    def runningSum(self, nums: List[int]) -> List[int]:

        if len(nums) >= 1:
            global a
            a = 0
            res = []
            a = nums[0] + a
            nums = nums[1:]
            res.append(a)
            runningSum(self, nums)
        return res
    """

"""helper"""
class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        def helper(nums1,i):
            nums1[i] += nums1[i-1]
            if i==(len(nums1)-1):
                print(nums,i)
                return nums1
            else:
                return helper(nums1,i+1)
        return helper(nums,1)


