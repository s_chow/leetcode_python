"""
Given an integer array nums, move all the even integers at the beginning of the array followed by all the odd integers.
Return any array that satisfies this condition.
Example 1:
Input: nums = [3,1,2,4]
Output: [2,4,3,1]
Explanation: The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.
"""
""""Simply iterate the array keeping the index of the last known odd number. If arr[i] is even, swap it with arr[last_odd_idx]. This will ensure that the odd numbers 'bubble' to the end of the array and as a bonus we keep the same array structure (i.e., [1,3,2,4] becomes [2,4,1,3])
Time complexity: O(N)
Space complexity: O(1)"""

def sortArrayByParity(self, nums: List[int]) -> List[int]:

    last_odd_idx = 0

    for i in range(len(nums)):
        is_even = nums[i] % 2 == 0
        if is_even:
            nums[i], nums[last_odd_idx] = nums[last_odd_idx], nums[i]
            last_odd_idx += 1

    return nums

"""
def sortArrayByParity(self, A):
    return [a for a in A if a % 2 == 0] + [a for a in A if a % 2]
"""