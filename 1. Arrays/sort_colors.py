"""
Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white, and blue.
We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.
You must solve this problem without using the library's sort function.
Example 1:
Input: nums = [2,0,2,1,1,0]
Output: [0,0,1,1,2,2]
"""
class Solution:

    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """

        # constant for colors
        RED, WHITE, BLUE = 0, 1, 2

        # two pointers for RED as well as BLUE
        idx_red, idx_blue = 0, len(nums)-1

        i = 0
        while i <= idx_blue :

            if nums[i] == RED:

                nums[idx_red], nums[i] = nums[i], nums[idx_red]

                # update idx for red
                idx_red += 1


            elif nums[i] == BLUE:

                nums[idx_blue], nums[i] = nums[i], nums[idx_blue]

                # update idx for blue
                idx_blue -= 1

                # i-1 in order to stay and do one more color check on next iteration
                i -= 1


            # i move forward
            i += 1


"""
# count sort    
def sortColors1(self, nums):
    c0 = c1 = c2 = 0
    for num in nums:
        if num == 0:
            c0 += 1
        elif num == 1:
            c1 += 1
        else:
            c2 += 1
    nums[:c0] = [0] * c0
    nums[c0:c0+c1] = [1] * c1
    nums[c0+c1:] = [2] * c2
   
# one pass 
def sortColors(self, nums):
    # zero and r record the position of "0" and "2" respectively
    l, r, zero = 0, len(nums)-1, 0
    while l <= r:
        if nums[l] == 0:
            nums[l], nums[zero] = nums[zero], nums[l]
            l += 1; zero += 1
        elif nums[l] == 2:
            nums[l], nums[r] = nums[r], nums[l]
            r -= 1
        else:
"""

"""
The problem is called dutch national flag problem.

Gist below:

a) Traverse from left to right
b) Maintain the most recent position of 0 and the position of 2.
c) When 0 is encountered move to the left and for 2 move to right. (increment/decrement the pointers accordingly)
d) When 1 is encountered do nothing.

Code below:

def sortColors(self, A):

    leftindex = zeropos = 0
    right_index = len(A) - 1

    while left_index <= right_index:
        if A[left_index] == 0:
           A[left_index], A[zero_pos] = A[zero_pos], A[left_index]
           left_index += 1
           zero_pos += 1
        elif A[left_index] == 2:
           A[right_index], A[left_index] = A[left_index], A[right_index]
           right_index -= 1
        else:
           left_index += 1
"""