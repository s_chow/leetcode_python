"""
Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
Input: nums = [-4,-1,0,3,10]
Output: [0,1,9,16,100]
Explanation: After squaring, the array becomes [16,1,0,9,100].
After sorting, it becomes [0,1,9,16,100].
"""
from typing import List
""" #More organic way
class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        l = len(nums) - 1
        result = [0]*(l+1)
        left, right = 0, l
        for i in range(l, -1, -1):
            if abs(nums[left]) < abs(nums[right]):
                result[i] = nums[right]**2
                right -= 1
            else:
                result[i] = nums[left]**2
                left += 1
        return result
"""
class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        for i in range(len(nums)):
            nums[i] = nums[i] * nums[i]
        nums.sort(reverse = False)
        return nums

"""
class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
         return sorted([num*num for num in nums])
"""
obj: Solution = Solution()
print(obj.sortedSquares([-4, 2, 3, 1]))
print(obj.sortedSquares([1, 2, 3, 4]))
print(obj.sortedSquares([]))

