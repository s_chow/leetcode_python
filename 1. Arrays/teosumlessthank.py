"""
Given an array nums of integers and integer k, return the maximum sum such that there exists i < j with nums[i] + nums[j] = sum and sum < k. If no i, j exist satisfying this equation, return -1.
Input: nums = [34,23,1,24,75,33,54,8], k = 60
Output: 58
Explanation: We can use 34 and 24 to sum 58 which is less than 60.
"""
class Solution:
    def twoSumLessThanK(self, nums: List[int], k: int) -> int:
        numsorted = sorted(nums)
        front, back = 0, len(nums)-1
        curr_max = -1
        while (front < back):
            curr_sum = numsorted[front] + numsorted[back]
            if curr_sum >= k:
                back -= 1
            else:
                front += 1
            if curr_sum > curr_max and curr_sum < k:
                curr_max = curr_sum
        return curr_max