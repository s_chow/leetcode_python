 """Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1]. """

"""BRUTE FORCE: 
Time complexity - o(n2)
Space complexity - o(1)"""

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(0, len(nums)):
            for j in range(i+1, len(nums)):
                if nums[i] + nums[j] == target:
                    return [i, j]

"""TWO PASS HASH MAP:
Time complexity - o(n)
Space complexity - o(n)"""


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        hashmap = {}
        for i in range(0, len(nums)):
            hashmap[nums[i]] = i # {2->0, 7 ->1, 11->2, 15->3}
        for i in range(len(nums)):
            complement = target - nums[i]
            if complement in hashmap and hashmap[complement] != i:
                return [i, hashmap[complement]] 
                    
"""ONE PASS HASH MAP:
Time complexity - o(n)
Space complexity - o(n)  
"""
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        hashmap = {}
        for i in range(len(nums)):
            complement = target - nums[i]
            if complement in hashmap:
                return [i, hashmap[complement]]
            hashmap[nums[i]] = i

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        indice = {}
        for i, v in enumerate(nums):
            if v in indice:
                return [indice[v], i]
            else:
                indice[target - v] = i

"""
Time complexity - o(n)
Space complexity - o(1)"""

 class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for idx, num in enumerate(nums):
            if target - num in nums[idx+1:]:
                return [idx, nums[idx+1:].index(target-num) +idx+1]


""" Python code
 idx+1 is imp in the end
 >>> nums = [2,7,11,15]
 >>> a = nums[1:]
 >>> a
 [7, 11, 15]
 >>> a.index(7)
 0
 
>>> nums = [11,12, 13, 14]
>>> hm= {}
>>> hm[nums[0]] = 0
>>> hm
{11: 0}
>>> 

>>> for i in range(0,3):
	print(i)
0
1
2

 """