"""
Given an array of integers arr, return true if and only if it is a valid mountain array.
Recall that arr is a mountain array if and only if:
arr.length >= 3
There exists some i with 0 < i < arr.length - 1 such that:
arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
Input: arr = [2,1]
Output: false
"""
"""
For that one we will use the help of 2 pointers one will start from left & another will start from right. If left and right meets on same index value then we return true, because it's a stricly increasing and decreasing mountain.
"""
class Solution:
    def validMountainArray(self, arr: List[int]) -> bool:
        left = 0
        right = len(arr)-1
        if len(arr) <3:
            return False
        while left + 1< len(arr)-1 and arr[left] < arr[left+1]:
            left += 1
        while right - 1 > 0 and arr[right] < arr[right-1]:
            right -= 1
        return left == right

"""
We go up (inc=True), then down.

def validMountainArray(self, arr: List[int]) -> bool:
	if len(arr) < 3 or arr[0] >= arr[1]: return False

	inc = True
	for i in range(1, len(arr)):
		if arr[i] == arr[i-1]:
			return False
		elif inc == (arr[i] > arr[i-1]):
			continue
		elif not inc:
			return False
		else:
			inc = False

	return not inc
"""






