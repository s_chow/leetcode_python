"""
def nto1(n:int):
    if n != 0:
        print(n)
        return nto1(n-1)


def oneton(n:int):
    if n != 0:
        oneton(n-1)
    print(n)
"""
def nto1(n:int):
    if n == 0:
        return
    print(n)
    nto1(n-1)


def oneton(n:int):
    if n == 0:
        return
    oneton(n-1)
    print(n) #writing after recursive function stores after closing the function

nto1(4)
""" You dont have to use return function in here"""
oneton(4)

"""The reason this works is that recursive calls go on the call stack. As you push calls onto the stack, while your end case isn't met, you'll keep adding more calls until you reach your base case of n == 0, and then you'll exclusively start printing the values.
The other calls will then fall through to the print statement, as their execution has returned to the line after the conditional.
So, the call stack looks something like this:
countdown(5)
    countdown(4)
        countdown(3)
            countdown(2)
                countdown(1)
                    countdown(0)
                    print(0)
                print(1)
            print(2)
        print(3)
    print(4)
print(5) """

