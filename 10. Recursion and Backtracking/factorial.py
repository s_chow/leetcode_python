def fact(n):
    if (n == 1):
        return 1
    return n * fact(n-1)

"""
5! = 
5*4! = 5 * 4 * 3 
return is used because factorial function returns an integer
"""

print(fact(5))