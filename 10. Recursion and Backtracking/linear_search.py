from typing import List
"""
def search(num: List, target: int, i:int) -> int:
    if target == num[i]:
        return i
    if i == len(num)-1:
        return -1
    return search(num, target, i+1)

print(search([1, 3, 8, 9, 34, 23], 23, 0))"""

from typing import List
def searchmultiple(num: List, target: int, i:int) -> List:
    res = [] #not very optimised because creating new space
    if target == num[i]:
        res.append(i)
    if i == len(num)-1:
        return res
    ansfrombelowcalls = searchmultiple(num, target, i+1)
    res.extend(ansfrombelowcalls)
    return res

"""index 0 - []
    index 1 - [1]
        index 2 - []
            index 3 - [3]"""

print(searchmultiple([1,23 ,9, 23], 23, 0))