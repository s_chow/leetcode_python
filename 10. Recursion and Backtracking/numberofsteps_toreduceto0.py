def numberOfSteps(num: int) -> int:
    return helper(num, c=0)

def helper(num, c):
    if num == 0:
        return c
    if num % 2 == 0:
        return helper(num//2, c+1)
    return helper((num-1), c+1)

print(numberOfSteps(14))
"""
class Solution:
    def numberOfSteps (self, num: int) -> int:
        count = 0
        while num>0:
            if num%2 == 0:
                num = num/2
            else:
                num = (num-1)
            count+=1      
        return count
        """