from typing import List
from xmlrpc.client import boolean


def sorted_array(nums: List, i: int) -> boolean:
    if i == len(nums) -1 :
        return True
    return nums[i] < nums[i+1] and sorted_array(nums, i+1)

print(sorted_array([1, 2, 3, 6, 5], 0))



