"""
There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at any point in time.
Given the two integers m and n, return the number of possible unique paths that the robot can take to reach the bottom-right corner.
The test cases are generated so that the answer will be less than or equal to 2 * 109
Input: m = 3, n = 7
Output: 28
Input: m = 3, n = 2
Output: 3
Explanation: From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -> Down -> Down
2. Down -> Down -> Right
3. Down -> Right -> Down
"""

"""
By simple observation, one can observe than we can only reach cell (m, n), by firstly reaching either (m - 1, n) or (m, n - 1). 
So, this means the number of possible ways of reaching the goal cell (m, n) is the addition of the ways of reaching cell (m - 1, n) 
and (m, n - 1). Same applies to the later two cells, until we reach a known case. The known case here is when we have either (m, 1) 
or (1, n); In such cases there is only 1 possibility of reaching those cells (go all the way right or all the way bottom).

Finally, as many cases keep on re-appearing, we can either implement our own memoization, or use python's built in 
lru_cache (least recently used), which is a memory efficient way of skipping repeated calculations.
"""

from functools import lru_cache
class Solution:
    @lru_cache(maxsize=1024)
    def uniquePaths(self, m: int, n: int) -> int:
        if (m == 1 or n == 1):
            return 1
        left = self.uniquePaths(m-1, n)
        right = self.uniquePaths(m, n-1)
        return left + right

"""
def uniquePaths(self, m, n):
    return math.comb(m + n - 2, m - 1)
"""

"""
Dynamic programming: O(mn) time O(mn) space

class Solution(object):
    def uniquePaths(self, m, n):
        """
:type m: int
:type n: int
:rtype: int
"""
table = [[0]*n for _ in range(m)] #table = [[0 for x in range(n)] for x in range(m)]
for i in range(n):
    table[0][i] = 1
for j in range(m):
    table[j][0] = 1
for i in range(1,m):
    for j in range(1,n):
        table[i][j] = table[i-1][j] + table[i][j-1]
return table[m-1][n-1]
"""


"""
-- , x y-1
x-1 y, xy
Path count to [x,y] = path count to [x-1, y] + path count to [x, y-1]

method_#1: Dynamic Programming

class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        
        rows, cols = m, n
        
        path_dp = [ [ 1 for j in range(cols)] for i in range(rows) ]
        
        
        # Dynamic Programming relation:
        
        # Base case:
        # DP(0, j) = 1 , only reachable from one step left
        # DP(i, 0) = 1 , only reachable from one step up
        
        # General case:
        # DP(i,j) = number of path reach to (i, j)
        #         = number of path reach to one step left + number of path reach to one step up
        #         = number of path reach to (i, j-1) + number of path to (i-1, j)
        #         = DP(i, j-1) + DP(i-1, j)
        
        
        
        for i in range(1, rows):
            for j in range(1, cols):
                
                path_dp[i][j] = path_dp[i][j-1] + path_dp[i-1][j]
        
        
        # Destination coordination = (rows-1, cols-1)
        return path_dp[rows-1][cols-1]
        
        


"""