"""Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
Example 1:
Input: nums = [1,2,3,1]
Output: true """   

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        """
        from collections import Counter
        dict = Counter(nums)
        for x in dict.values():
            if x > 1:
                return True
        """
        hashset = {}
        for x in nums:
            if x not in hashset:
                hashset[x] = 1
            else:
                return True