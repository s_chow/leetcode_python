"""387. First Unique Character in a String
Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.
Input: s = "leetcode"
Output: 0 """
from collections import Counter


class Solution:
    def firstUniqChar(self, s: str) -> int:
        l = Counter(s)
        for i in range(len(s)):
            if l[s[i]] == 1:
                return i
        return -1

obj: Solution = Solution()
print(obj.firstUniqChar("snehaysneha"))
print(obj.firstUniqChar("Leetcode"))
