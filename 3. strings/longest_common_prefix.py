"""
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".



Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.


Constraints:

1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lower-case English letters.

"""

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if not strs:
            return ""
        shortest = min(strs,key=len)
        for i, ch in enumerate(shortest):
            for other in strs:
                if other[i] != ch:
                    return shortest[:i]
        return shortest

"""

shortest = min(strs,key=len)

l1=[1,-4,5,-7,-9,2]
print (min(l1,key=abs))
#Output:1

print (min('a','A','B','c','Z',key=str.lower))
#Output:a

userdefined
def cmp_key(e):
    return e[1]
l1 = [(1, 2, 3), (2, 1, 3), (11, 4, 2), (9, 1, 3)]
# Based on user defined function
print(min(l1, key=cmp_key))
# Output:(2,1,3)
print(max(l1, key=cmp_key))
#Output:(11,4,2)

l1=['red','RED','Yellow','blue','pink']
#Without key function,it will compare based on lexicographic order
print (max(l1))#Output:red
print (min(l1))#Output:RED
print (max(l1,key=lambda x:x.lower()))#Output:Yellow
print (min(l1,key=lambda x:x.lower()))#Output:Blue

l1=[(1,2,3),(3,1,1),(8,5,3),(3,4,2)]

#Finding the largest/smallest item based on second element in the tuple.
# lambda function returns second element in the tuple
print (min(l1,key=lambda x:x[1]))
#Output:(3, 1, 1)
print (max(l1,key=lambda x:x[1]))
#Output:(8, 5, 3)
#Without key function
#by default it will take the first element in the tuple.
print (min(l1))#Output:(1, 2, 3)
print (max(l1))#Output:(8, 5, 3)


"""

