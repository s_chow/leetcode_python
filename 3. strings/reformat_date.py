"""
Given a date string in the form Day Month Year, where:

Day is in the set {"1st", "2nd", "3rd", "4th", ..., "30th", "31st"}.
Month is in the set {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}.
Year is in the range [1900, 2100].
Convert the date string to the format YYYY-MM-DD, where:

YYYY denotes the 4 digit year.
MM denotes the 2 digit month.
DD denotes the 2 digit day.
Input: date = "20th Oct 2052"
Output: "2052-10-20"
"""

class Solution:
    def reformatDate(self, date: str) -> str:
        dt = date.split(' ')
        mon = {"Jan" :1, "Feb":2, "Mar":3, "Apr":4, "May":5, "Jun":6, "Jul":7, "Aug":8, "Sep":9, "Oct":10, "Nov":11, "Dec":12}
        return str(dt[2])+ '-' + str(mon[dt[1]]).zfill(2) + '-' + str(dt[0][:-2]).rjust(2, '0')

