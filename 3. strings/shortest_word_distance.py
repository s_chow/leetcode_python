"""
Given an array of strings wordsDict and two different strings that already exist in the array word1 and word2, return the shortest distance between these two words in the list.

Example 1:

Input: wordsDict = ["practice", "makes", "perfect", "coding", "makes"], word1 = "coding", word2 = "practice"
Output: 3
Example 2:

Input: wordsDict = ["practice", "makes", "perfect", "coding", "makes"], word1 = "makes", word2 = "coding"
Output: 1
"""

def shortestDistance(self, words: List[str], word1: str, word2: str) -> int:
    shortestDistance = len(words)
    position1, position2 = -1, -1
    for i in range(len(words)):
        if words[i]==word1:
            position1 = i
        elif words[i]==word2:
            position2 = i

        if position1!=-1 and position2!=-1:
            shortestDistance = min(shortestDistance, abs(position1 - position2))

    return shortestDistance


class Solution(object):
    def shortestDistance(self, words, word1, word2):

        w1 = [i for i in xrange(len(words)) if words[i] == word1]
        w2 = [i for i in xrange(len(words)) if words[i] == word2]

        return min([abs(i - j) for i in w1 for j in w2])
looks good but notefficient


class Solution:
    def shortestDistance(self, wordsDict: List[str], word1: str, word2: str) -> int:
        size = len(wordsDict)
        index1, index2 = size, size
        ans = size

        for i in range(size):
            if wordsDict[i] == word1:
                index1 = i
                ans = min(ans, abs(index1-index2))
            elif wordsDict[i] == word2:
                index2 = i
                ans = min(ans, abs(index1-index2))
        return ans

