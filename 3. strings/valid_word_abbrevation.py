"""408. Valid Word Abbreviation

A string can be abbreviated by replacing any number of non-adjacent, non-empty substrings with their lengths. The lengths should not have leading zeros.

For example, a string such as "substitution" could be abbreviated as (but not limited to):

"s10n" ("s ubstitutio n")
"sub4u4" ("sub stit u tion")
"12" ("substitution")
"su3i1u2on" ("su bst i t u ti on")
"substitution" (no substrings replaced)
The following are not valid abbreviations:

"s55n" ("s ubsti tutio n", the replaced substrings are adjacent)
"s010n" (has leading zeros)
"s0ubstitution" (replaces an empty substring)
Given a string word and an abbreviation abbr, return whether the string matches the given abbreviation.

A substring is a contiguous non-empty sequence of characters within a string.

word = "internationalization", abbr = "i12iz4n"
"""

class Solution:
    def validWordAbbreviation(self, word: str, abbr: str) -> bool:
        wordindex, abbrindex = 0, 0
        wordlen, abbrlen = len(word), len(abbr)

        while wordindex < wordlen and abbrindex < abbrlen:
            if word[wordindex] == abbr[abbrindex]:
                wordindex += 1
                abbrindex += 1
                continue

            if not abbr[abbrindex].isnumeric() or abbr[abbrindex] == '0':
                return False

            start = abbrindex
            while abbrindex < abbrlen and abbr[abbrindex].isnumeric():
                abbrindex += 1

            numtoskip = int(abbr[start:abbrindex])
            wordindex += numtoskip

        return wordindex == wordlen and abbrindex == abbrlen