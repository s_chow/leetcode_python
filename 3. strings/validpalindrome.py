"""A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward. Alphanumeric characters include letters and numbers.
Given a string s, return true if it is a palindrome, or false otherwise.
Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome."""

class Solution:
    def isPalindrome(self, s: str) -> bool:
        if not s:
            return True
        left, right = 0, len(s)-1
        while left < right:
            str_left = s[left].lower()
            str_right = s[right].lower()
            if str_left.isalnum() and str_right.isalnum():
                if str_left != str_right:
                    return False
                left += 1
                right -= 1
            else:
                if not str_left.isalnum():
                    left += 1
                if not str_right.isalnum():
                    right -= 1
        return True
"""
import re
class Solution:
    def isPalindrome(self, s: str) -> bool:
        s = re.sub("[^a-z0-9]",'',s.lower())
        return s == ''.join(reversed(s))
        """