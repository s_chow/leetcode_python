"""680. Valid Palindrome II
Given a string s, return true if the s can be palindrome after deleting at most one character from it.
Example 1:
Input: s = "aba"
Output: true"""

class Solution:
    def validPalindrome(self, s: str) -> bool:
        def isPalin(s):
            return s == s[::-1]

        for i in range(int(len(s)/2)):
            if s[i] != s[len(s)-i-1]:
                return isPalin(s[i:len(s)-i-1]) or isPalin(s[i+1:len(s)-i])
        return True