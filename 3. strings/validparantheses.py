"""
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
An input string is valid if:
Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Input: s = "()"
Output: true
Example 2:
Input: s = "()[]{}"
Output: true
Input: s = "(]"
Output: false
"""


class Solution:
    def isValid(self, s: str) -> bool:
        closure = {')': '(', '}': '{', ']': '['}
        opener = ['(', '{', '[']
        opened = []
        for c in list(s):
            if c in opener:
                opened.append(c)
            if c in closure:
                if closure[c] in opened and len(opened) > 0:
                    if opened[-1] == closure[c]:
                        opened.pop(-1)
                else:
                    return False
        return len(opened) == 0
