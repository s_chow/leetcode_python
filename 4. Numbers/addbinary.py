"""67. Add Binary - Given two binary strings a and b, return their sum as a binary string.
Example 1:
Input: a = "11", b = "1"
Output: "100"
Example 2:
Input: a = "1010", b = "1011"
Output: "10101" """

class Solution:
    def addBinary(self, a: str, b: str) -> str:
        return bin(int(a, 2) + int(b,2))[2:]
#int(str, base) - int(base is 2 for binary) --converts binaries to int

"""
>>> a = "1010"
>>> c = int(a, 2)
>>> c
10
>>> bin(c)
'0b1010'
>>> 
"""