"""
Given an integer x, return true if x is palindrome integer.
An integer is a palindrome when it reads the same backward as forward.
For example, 121 is a palindrome while 123 is not.
Example 1:
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
"""
class Solution:
    def isPalindrome(self, x: int) -> bool:
        reverse = 0
        num = x
        while(x >0):
            last_digit = x % 10
            reverse = reverse * 10 + last_digit
            x = x // 10
        if ( num == reverse):
            return True
        return False


# if x < 0:
#        return False
#    return x==int(str(x)[::-1])