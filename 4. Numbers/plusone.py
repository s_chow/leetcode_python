"""You are given a large integer represented as an integer array digits, where each digits[i] is the ith digit of the integer. The digits are ordered from most significant to least significant in left-to-right order. The large integer does not contain any leading 0's.
Increment the large integer by one and return the resulting array of digits.
Input: digits = [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Incrementing by one gives 123 + 1 = 124.
Thus, the result should be [1,2,4]

>>> list(range(10, -1, -1))
[10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]"""
from typing import List
class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        for idx in range(len(digits)-1, -1, -1):
            if digits[idx] != 9:
                digits[idx] += 1
                break
            else:
                digits[idx] = 0
        if digits[0] == 0:
            digits.insert(0, 1)
        return digits

obj = Solution()
print(obj.plusOne([1, 2, 7, 9]))
print(obj.plusOne([1, 2, 9]))
"""
class Solution:
    def plusOne(self, digits):
        
:type digits: List[int]
:rtype: List[int]

carry = 1
for i in range(len(digits)-1, -1, -1):
    carry, digits[i] = divmod(digits[i]+carry, 10)
    if carry == 0:
        return digits
return [1]+digits
"""