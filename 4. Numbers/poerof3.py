"""
Given an integer n, return true if it is a power of three. Otherwise, return false.
An integer n is a power of three, if there exists an integer x such that n == 3x.
Input: n = 27
Output: true
"""
class Solution:
    def isPowerOfThree(self, n: int) -> bool:
        while n > 3:
            n /= 3
        if n != int(n):
            return False
        if n in {1, 3}:
            return True
        return False

