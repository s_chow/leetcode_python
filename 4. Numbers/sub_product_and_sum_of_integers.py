"""
Given an integer number n, return the difference between the product of its digits and the sum of its digits.

Example 1:
Input: n = 234
Output: 15
Explanation:
Product of digits = 2 * 3 * 4 = 24
Sum of digits = 2 + 3 + 4 = 9
Result = 24 - 9 = 15
Example 2:

Input: n = 4421
Output: 21
Explanation:
Product of digits = 4 * 4 * 2 * 1 = 32
Sum of digits = 4 + 4 + 2 + 1 = 11
Result = 32 - 11 = 21
"""

class Solution:
    def subtractProductAndSum(self, n: int) -> int:
        product = 1
        summ = 0
        while n > 0:
            last_digit = n%10
            n = n//10
            product = product * last_digit
            summ = summ + last_digit
        return product-summ

""" 
def subtractProductAndSum(n)t:
    return eval('*'.join(str(n))) - eval('+'.join(str(n)))
    """

"""  
def subtractProductAndSum(self, n: int) -> int:
    sum, prod = 0, 1
    while n:
        n, digit = divmod(n, 10)
        sum += digit
        prod *= digit
    return prod - sum
Analysis:
Time: O(logn), space: O(1).
"""

