"""1290. Convert Binary Number in a Linked List to Integer
Input: head = [1,0,1]
Output: 5
Explanation: (101) in base 2 = (5) in base 10

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next"""
class Solution:
    def getDecimalValue(self, head: ListNode) -> int:
        value = 0
        while head:
            value = 2 * value + head.val
            head = head.next
        return value