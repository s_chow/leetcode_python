"""1474. Delete N Nodes After M Nodes of a Linked List
You are given the head of a linked list and two integers m and n.
Traverse the linked list and remove some nodes in the following way:
Start with the head as the current node.
Keep the first m nodes starting with the current node.
Remove the next n nodes
Keep repeating steps 2 and 3 until you reach the end of the list.
Return the head of the modified list after removing the mentioned nodes.

Input: head = [1,2,3,4,5,6,7,8,9,10,11,12,13], m = 2, n = 3
Output: [1,2,6,7,11,12]

Logic 1: follow the prob steps, at every start node, skip some and delete others, jump to a new start and then start the loop again - 100 pass - 78% faster

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next"""
class Solution:
    def deleteNodes(self, head: ListNode, m: int, n: int) -> ListNode:
        # Store head ref to return
        result = head
        # Iterate until end
        while head:
            # Refresh n, m at every start node
            noDel = m-1
            delete = n
            # no delete m
            while head and noDel:
                head = head.next
                noDel -= 1
            # delete n
            while head and head.next and delete:
                head.next = head.next.next
                delete -= 1
            # move to a new start
            if head:
                head = head.next
        return result