"""876. Middle of the Linked List
Given the head of a singly linked list, return the middle node of the linked list.
If there are two middle nodes, return the second middle node.
1st traverse the entire linkes list get the total number of nodes. now divide the n/2 + 1 now traberse again till this point - time complexity o(n) + o(n/2)
so we use tortoise method -  slow and fast - move the slow by 1 and fast one by 2 till u reach end if they ask u to return the 1st middle one then store the previous slow tortoise value
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next"""
class Solution:
    def middleNode(self, head):
        fast = slow = head
        while fast and fast.next: #fast and fast.next because if its even linked list it crosses the linked list
            fast, slow = fast.next.next, slow.next
        return slow


"""test:
class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        slow = head
        fast = head
        while fast.next and fast.next.next:
            slow = slow.next
            if fast.next and fast.next.next:
                fast = fast.next.next

        if fast.next:
            return slow.next
        return slow"""