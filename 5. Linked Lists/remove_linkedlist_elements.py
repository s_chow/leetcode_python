"""203. Remove Linked List Elements
Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.
Input: head = [1,2,6,3,4,5,6], val = 6
Output: [1,2,3,4,5]

# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next"""
class Solution:
    def removeElements(self, head: Optional[ListNode], val: int) -> Optional[ListNode]:

        while(head != None and head.val == val):   #if its the 1st element
            head = head.next

        prev = head
        curr = head

        while(curr != None):
            if curr.val == val:
                prev.next = curr.next
            else:
                prev = curr
            curr = curr.next

        return head