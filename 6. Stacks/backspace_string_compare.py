"""844. Backspace String Compare
Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.
Note that after backspacing an empty text, the text will continue empty.

Example 1:
Input: s = "ab#c", t = "ad#c"
Output: true
Explanation: Both s and t become "ac"."""

class Solution:
    def backspaceCompare(self, s: str, t: str) -> bool:
        res1 = []
        res2 = []
        for a in list(s):
            if a != '#' :
                res1.append(a)
            elif(len(res1) > 0):
                res1.pop()

        for a in list(t):
            if a != '#' :
                res2.append(a)
            elif(len(res2) > 0):
                res2.pop()

        if res1 == res2:
            return True
        else:
            return False


"""
def backspaceCompare(self, S: str, T: str) -> bool:
    def strip_str(v: str) -> str:
        while '#' in v:
            i = v.index('#')
            if i == 0:
                v = v[i+1:len(v)]
                continue
            v = v[:i-1] + v[i+1:len(v)]
        return v
    return strip_str(S) == strip_str(T)"""