"""716. Max Stack
Design a max stack data structure that supports the stack operations and supports finding the stack's maximum element.

Implement the MaxStack class:

MaxStack() Initializes the stack object.
void push(int x) Pushes element x onto the stack.
int pop() Removes the element on top of the stack and returns it.
int top() Gets the element on the top of the stack without removing it.
int peekMax() Retrieves the maximum element in the stack without removing it.
int popMax() Retrieves the maximum element in the stack and removes it. If there is more than one maximum element, only remove the top-most one.
"""

class MaxStack:

    def __init__(self):
        self.maxstack = []

    def push(self, x: int) -> None:
        self.maxstack.append(x)

    def pop(self) -> int:
        return self.maxstack.pop()

    def top(self) -> int:
        return self.maxstack[-1]

    def peekMax(self) -> int:
        return max(self.maxstack)

    def popMax(self) -> int:
        popmax = self.peekMax()
        for i in range(-1, -len(self.maxstack) - 1, -1):
            if self.maxstack[i] == popmax:
                del self.maxstack[i]
                break
        return popmax

# Your MaxStack object will be instantiated and called as such:
# obj = MaxStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.peekMax()
# param_5 = obj.popMax()