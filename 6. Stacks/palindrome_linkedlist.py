"""234. Palindrome Linked List
Given the head of a singly linked list, return true if it is a palindrome.
Input: head = [1,2,2,1]
Output: true
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next"""
class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        if head == None:
            return None

        tempStack = []
        temp = head
        while temp:
            tempStack.append(temp.val)
            temp = temp.next

        while head:
            if head.val != tempStack.pop():
                return False
            head = head.next

        return True