graph: node -- edge
undirected graph  -- > facebook friends connections
directed graph --> flights from one place to other
weighted graph -->

Nodes can be cities, links, etc.. for making it easy we will number the nodes from 0 to n-1(so that we can access them directly in adjacency list by indexing)
edge is a pair (1,2) i.e edge is present between 1 and 2. edge counld be bidirectional as well

to represent graph in proper way - adjacency list - for each node it contains list of all neighbors. --> each edge appears twice in this case

whenever you want to create a list of empty list do not use [[]] * 10 --> this creates 10 copies when you assign to 1st one it replicatesto everything. but it works with [0] * 10

>>> [0]*10
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
>>> a = [0] * 10
>>> a
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
>>> a[0] = 1
>>> a
[1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
>>> a[1] = 2
>>> a[2] = 3
>>> a[3] = 4
>>> a
[1, 2, 3, 4, 0, 0, 0, 0, 0, 0]
>>> b = [[]] * 10
>>> b
[[], [], [], [], [], [], [], [], [], []]
>>> b[0] = 1
>>> b
[1, [], [], [], [], [], [], [], [], []]
>>> b[0].append(1)
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
AttributeError: 'int' object has no attribute 'append'
>>> b[1].append(2)
>>> b
[1, [2], [2], [2], [2], [2], [2], [2], [2], [2]]

so use below method

>>> a
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> a = [[] for x in range(10)]
>>> a
[[], [], [], [], [], [], [], [], [], []]
>>> a[0].append(1)
>>> a
[[1], [], [], [], [], [], [], [], [], []]

if variable is not being used use _
eg a = [[] for _ in range(10)]


>>> a = range(10)
>>> a
range(0, 10)
>>> a = list(range(10))
>>> a
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

>>> a = enumerate([1, 2, 3])
>>> a
<enumerate object at 0x0000013EDAAA1EC0>
>>> for x in a: print(x)
...
(0, 1)
(1, 2)
(2, 3)

BFS - discover the shortest path
in tree there is only one path between two nodes.
Eg: internet, facebook, amazon product recommendation is by graph

class Graph:
    def __init__(self, edges):
        self.edges = edges
        self.graph_dict = {}
        for start, end in edges:
            if start in self.graph_dict:
                self.graph_dict[start].append(end)
            else:
                self.graph_dict[start] = [end]
        print("Graph Dict:", self.graph_dict)

    def get_paths(self, start, end, path=[]):
        path = path + [start]

        if start == end:
            return [path]

        if start not in self.graph_dict:
            return []

        paths = []
        for node in self.graph_dict[start]:
            if node not in path:
                new_paths = self.get_paths(node, end, path)
                for p in new_paths:
                    paths.append(p)

        return paths

    def get_shortest_path(self, start, end, path=[]):
        path = path + [start]

        if start == end:
            return path

        if start not in self.graph_dict:
            return None

        shortest_path = None
        for node in self.graph_dict[start]:
            if node not in path:
                sp = self.get_shortest_path(node, end, path)
                if sp:
                    if shortest_path is None or len(sp) < len(shortest_path):
                        shortest_path = sp

        return shortest_path

if __name__ == '__main__':

    routes = [
        ("Mumbai","Pune"),
        ("Mumbai", "Surat"),
        ("Surat", "Bangaluru"),
        ("Pune","Hyderabad"),
        ("Pune","Mysuru"),
        ("Hyderabad","Bangaluru"),
        ("Hyderabad", "Chennai"),
        ("Mysuru", "Bangaluru"),
        ("Chennai", "Bangaluru")
    ]

    routes = [
        ("Mumbai", "Paris"),
        ("Mumbai", "Dubai"),
        ("Paris", "Dubai"),
        ("Paris", "New York"),
        ("Dubai", "New York"),
        ("New York", "Toronto"),
    ]

    route_graph = Graph(routes)

    start = "Mumbai"
    end = "New York"

    print(f"All paths between: {start} and {end}: ",route_graph.get_paths(start,end))
    print(f"Shortest path between {start} and {end}: ", route_graph.get_shortest_path(start,end))

    start = "Dubai"
    end = "New York"

    print(f"All paths between: {start} and {end}: ",route_graph.get_paths(start,end))
    print(f"Shortest path between {start} and {end}: ", route_graph.get_shortest_path(start,end))

###################################################################################################

def findCenter(self, e: List[List[int]]) -> int:
    return (set(e[0]) & set(e[1])).pop()

def findCenter(self, e: List[List[int]]) -> int:
    if e[0][0]==e[1][0] or e[0][0]==e[1][1]:
        return e[0][0]
    return e[0][1]

def findCenter(self, e: List[List[int]]) -> int:
    return e[0][e[0][1] in e[1]]

##################################################################################################
unionfind

class Solution:
    def validPath(self, n: int, edges: List[List[int]], start: int, end: int) -> bool:
        def find(n):
            while n != parent[n]:
                n = parent[n]
            return n

        parent = list(range(n))
        for n1, n2 in edges:
            p1 = find(n1)
            p2 = find(n2)
            parent[p1] = p2

        return find(start) == find(end)

DFS -  All DFS solutions use recursion, which uses the function call stack. BFS does not use recursion, it uses a queue instead.
class Solution:
    def validPath(self, n: int, edges: List[List[int]], start: int, end: int) -> bool:
        neighbors = defaultdict(list)
        for n1, n2 in edges:
            neighbors[n1].append(n2)
            neighbors[n2].append(n1)

        def dfs(node, end, seen):
            if node == end:
                return True
            if node in seen:
                return False

            seen.add(node)
            for n in neighbors[node]:
                if dfs(n, end, seen):
                    return True

            return False

        seen = set()
        return dfs(start, end, seen)
DFS

from collections import defaultdict
class Solution:
    def validPath(self, n: int, edges: List[List[int]], start: int, end: int) -> bool:
        adj_list = defaultdict(list)
        for k, v in edges:
            adj_list[k].append(v)
            adj_list[v].append(k)
        visited = set()
        def dfs(node):
            if node == end: return True
            if node not in visited:
                visited.add(node)
                for edge in adj_list[node]:
                    res = dfs(edge)
                    if res: return True
        return dfs(start)

BFS

def validPath(self, n: int, edges: List[List[int]], start: int, end: int) -> bool:
    neighbors = defaultdict(list)
    for n1, n2 in edges:
        neighbors[n1].append(n2)
        neighbors[n2].append(n1)

    q = deque([start])
    seen = set([start])
    while q:
        node = q.popleft()
        if node == end:
            return True
        for n in neighbors[node]:
            if n not in seen:
                seen.add(n)
                q.append(n)

    return False

practice:
from collections import defaultdict
>>> edges = [[0,1],[1,2],[2,0]]
>>> neighbors = defaultdict(list)
>>> for n1, n2 in edges:
    ...             neighbors[n1].append(n2)
...             neighbors[n2].append(n1)
...
>>> neighbors
defaultdict(<class 'list'>, {0: [1, 2], 1: [0, 2], 2: [1, 0]})

