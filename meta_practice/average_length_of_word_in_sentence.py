find the average length of word in sentence
Calculate the average word length - some of the edge cases are having spaces in the beginning and
end of the words - returning a float instead of int, returning None for blank input.

sentence = "Hi my name is Bob"
words = sentence.split()
print(words)
average= 1.0 * sum(len(word) for word in words)/len(words)
print(average)