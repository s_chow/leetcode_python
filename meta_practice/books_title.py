"""
Loops
for x in l:  "Iterate on x for each value in list"
for i in range(0,5): "Iterate on i from value 0 to 4"
for k, v in d.items(): "Iterate on each key, value pair in dict"

Lists (Array)
l = []  "Define an empty list"
l[i]  "Return value at index i in list"
len(l) "Return length of list"
l.append(x) "Add value x to the end of list"
l.sort() "Sort values in list - in place sort, returns None"
sorted(l) "Return sorted copy of list"
x in l: "Evaluate True if x is contained in the list"

Dictionary (HashMap)
d = {}  "Define an empty Dictionary"
d[x]  "Return value for key x"
d[x] = 1 "Set value for key x to 1"
d.keys()  "Return list of keys"
d.values() "Return list of values"

Other functions
reversed(n)  "reverse a list"
split()    "returns a list of all the words in the string"
"""

# Given a list of book titles, find all editions of same book. Edition of a book
# has full title of original book as prefix in its title.

# Example: original book title is “Green Trees”, editions can be titled as
# “Green Trees Revised”, “Green Trees Online Edition”, “Green Trees for Dummies” etc.

# example input: ["Green Trees", "Python in a Nutshell", "Python", "Red Trees"]
# expected output: ["Python in a Nutshell"]
# explanation: book "Python in a Nutshell" is an edition of original book "Python"
# since "Python in a Nutshell" starts with "Python".
# on contrary "Red Trees" isn't an edition of "Green Trees" since one isn't a
# prefix for another. Hence only "Python in a Nutshell" is an edition.


from typing import List

def find_editions(books: List[str]) -> List[str]:
    books = sorted(books, reverse =True)
    res = {}
    cnt = []
    for book in books:
        for k, v in res.items():
            if k.startswith(book):
                cnt.append(k)
        res[book] = 0
    return cnt


assert(find_editions(["Green Trees", "Python in a Nutshell"]) == [])
assert(sorted(find_editions(["Green Trees", "Green Trees Second Edition", "Python in a Nutshell", "Python for Dummies", "Python"])) == sorted(['Python in a Nutshell', 'Python for Dummies', 'Green Trees Second Edition']))
assert(find_editions(["Python", "All About Pythons"]) == [])
print("all tests passed")