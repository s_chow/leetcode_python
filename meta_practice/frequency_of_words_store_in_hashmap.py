from typing import List
def freq(list:List) -> dict:
    res = {}
    for word in list:
        res[word] = res.get(word, 0) + 1
    return res

print(freq(['Apple','Mango', 'Apple', 'Orange', 'Orange', 'Orange']))