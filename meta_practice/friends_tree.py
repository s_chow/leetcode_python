"""
. - Facebook Friends TreeFriends problem [[A,B], [B,D],[E]...] ( List of lists); You have a 2-D array of friends like
[[A,B],[A,C],[B,D],[B,C],[R,M], [S],[P], [A]]
Write a function that creates a dictionary of how many friends each person has. People can have 0 to many friends.
However, there won't be repeat relationships like [A,B] and [B,A] and neither will there be more than 2 people in a relationship
"""
"""
from collections import defaultdict

m = [['a','b'],['a','c'],['a','d'],['b','w'],['b','e'],['f'],['g']]
res = defaultdict(int)
for rel in m:
    for p in rel:
        if len(rel) > 1:
            res[p]  += 1
        else:
            res[p] = 0
print(res)


This is the O(1) complexity algorithm.

items=[['A','B'],['A','C'],['B','D'],['B','C'],['R','M'], ['S'],['P'], ['A']]  """
result=dict()
for item in items:
    if item[0] not in result.keys():
        result[item[0]]=len(item)-1
    elif item[0] in result.keys():
        result[item[0]]+=len(item)-1
print(result)



This solution is not o(1) but o(nk) where k is number of keys as result.keys() is a list and you are iterating over a list which is o(n)...
simply use if item[0] not in result which will make it o(n) ( since you still have to loop over items which makes it 0(n) )