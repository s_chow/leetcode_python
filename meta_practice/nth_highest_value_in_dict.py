d = {'a':5 , 'b':4, 'c':3, 'd':3, 'e':1}

x = sorted(((v,k) for k,v in d.items()))
print(x[-2][1])
print(x[-3][1])
result:

b
d
(would fail if dict doesn't have at least 3 items)

Or directly with key parameter (avoids data reordering)

x = sorted(d.items(),key=(lambda i: i[1]))
print(x[-2][0])
print(x[-3][0])
result:

b
d

import collections

d = {'a':5 , 'b':4, 'c':3, 'd':3, 'e':1}

dd = collections.defaultdict(list)

for k,v in d.items():
    dd[v].append(k)

x = sorted(dd.items())

print(x[-2])
print(x[-3])
result:

(4, ['b'])
(3, ['c', 'd'])

"""new_dict ={"google":12, "amazon":9, "flipkart":4, "gfg":13}
# maximum value
max = max(new_dict.values())
# iterate through the dictionary
max2 = 0
for v in new_dict.values():
    if(v>max2 and v<max):
        max2 = v
# print the second largest value
print(max2)

example_dict = {"mark": 13, "steve": 3, "bill": 6, "linus": 11}
# now directly print the second largest
# value in the dictionary
print("Output1:", sorted(example_dict.values())[-2])
# More than 1 keys with maximum value are present
example_dict = {"fb": 20, "whatsapp": 12, "instagram": 20, "oculus": 10}
print("Output2:", sorted(set(example_dict.values()), reverse=True)[-2])


Tv = {'BreakingBad':100, 'GameOfThrones':1292, 'TMKUC' : 88}
Keymax = max(zip(Tv.values(), Tv.keys()))[1]
print(Keymax)
"""
