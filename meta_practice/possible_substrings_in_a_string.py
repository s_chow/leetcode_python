Count number of possible substrings in a given string

count of non empty substrings is n*(n+1) / 2
if we include empty substring as also a substring then its n*(n+1)/2  + 1

For example: string str = "Hello World"; // length == 11
Lets begin by taking only one-character substring at time: H, e, l, l, o, , W, o, r, l, d. Then by taking 2 characters at time: He, el, ll, lo, o , W, Wo, or, rl, ld. Then by taking 3 chars: Hel, .. etc.
So the total substring count is 11 + 10 + 9 + .. + 1 and, in general, for i from 1 to n, we have n - i + 1 substrings. By summition:
    Sigma (n + 1 - i) from i = 1 to n, yields n * (n + 1) - n * (n + 1) / 2 which is n * (n + 1) / 2

Hello
H, e , l, l, o
He, el, ll, lo
Hel, ello, llo
Hell, ello

5  + 4 + 3 + 2 + 1 = n*(n+1)/2

def countNonEmptySubstr(str):
    n = len(str);
    return int(n * (n + 1) / 2);

# driver code
s = "abcde";
print (countNonEmptySubstr(s));
