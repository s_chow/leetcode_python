"""
Pretty much I need to write a program to check if a list has any duplicates and if it does it removes them and returns a new list with the items that weren't duplicated/removed. This is what I have but to be honest I do not know what to d

"""

>>> t = [1, 2, 3, 1, 2, 5, 6, 7, 8]
>>> t
[1, 2, 3, 1, 2, 5, 6, 7, 8]
>>> list(set(t))
[1, 2, 3, 5, 6, 7, 8]
>>> s = [1, 2, 3]
>>> list(set(t) - set(s))
[8, 5, 6, 7]

"""
As you can see from the example result, the original order is not maintained. As mentioned above, sets themselves are unordered collections,
 so the order is lost. When converting a set back to a list, an arbitrary order is created.

If order is important to you, then you will have to use a different mechanism. A very common solution for this is to rely on OrderedDict
 to keep the order of keys during insertion:


"""


>>> from collections import OrderedDict
>>> list(OrderedDict.fromkeys(t))
[1, 2, 3, 5, 6, 7, 8]

"""
Starting with Python 3.7, the built-in dictionary is guaranteed to maintain the insertion order as well, so you can also use that directly if you are on Python 3.7 or later (or CPython 3.6):
"""

>>> list(dict.fromkeys(t))
[1, 2, 3, 5, 6, 7, 8]

>>> list(dict.fromkeys('abracadabra'))
['a', 'b', 'r', 'c', 'd']