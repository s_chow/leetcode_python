"""
A balanced array would be an array in which each element appears the same number of times.
Given an array with n elements, return a dictionary with the key as the element and the value as the count of elements needed to balance the given array.
Signature
HashMap<Object, Integer> returnMissingBalancedNumbers(Object[] elements)
Input
Array of mixed elements (integers, strings, etc.)
Output
Object with a mixed key, and an integer value
Examples
elements: ["a", "b", "abc", "c", "a"]
output: {"b":1, "abc":1, "c":1}

elements: [1,3,4,2,1,4,1]
output: {2:2, 3:2, 4:1}

elements: [4,5,11,5,6,11]
output: {4:1,6:1}
"""

"""
Balance the string., i.e., How many additional literals needed to evenly balance the string.
String is made of a-j and 0-9 where alphabets marks the opening and numbers as closing.
For e.g., ab00a. This is unbalanced since it requires pair of b with 1, and additional 0 occurs before “a” ie., missing opening pair and later “a” without its closing number pair.. Result : 3
E.g., bj19 is balanced so result 0."""

import math
from collections import Counter
def return_missing_balanced_numbers(input):
    ip = Counter(input)
    val = max(ip.values())
    op = {}
    for i in ip.keys():
        if ip[i] != val:
            op[i] = val - ip[i]
    return op

"""
Given a list of ints, balance the list so that each int appears equally in the list. Return a dictionary where the key is the int and the value is the count neededto balance the list.
[1, 1, 2] => {2: 1}
[1, 1, 1, 5, 3, 2, 2] => {5: 2, 3: 2, 2: 1}

import collections
m = dict(collections.Counter(input))
mi = list(m.values())
mx= max(mi)
y=dict()
for key,val in m.items():
	if val < mx:
	y[key]=mx-val
print(y)
"""

# These are the tests we use to determine if the solution is correct.
# You can add your own at the bottom.

test_case_number = 1

def check(expected, output):
    global test_case_number
    result = False
    if expected == output:
        result = True
    rightTick = '\u2713'
    wrongTick = '\u2717'
    if result:
        print(rightTick, 'Test #', test_case_number, sep='')
    else:
        print(wrongTick, 'Test #', test_case_number, ': Expected ', sep='', end='')
        print(expected)
        print(' Your output: ', end='')
        print(output)
        print()
    test_case_number += 1

if __name__ == "__main__":

    # Testcase 1
    input_1 = ['a','b','abc','c','a']
    output_1 = return_missing_balanced_numbers(input_1)
    expected_1 = {'b':1,'abc':1,'c':1}
    check(expected_1, output_1)

    # Testcase 2
    input_2 = [1,3,4,2,1,4,1]
    output_2 = return_missing_balanced_numbers(input_2)
    expected_2 = {2:2,3:2,4:1}
    check(expected_2, output_2)

    # Testcase 3
    input_3 = [4,5,11,5,6,11]
    output_3 = return_missing_balanced_numbers(input_3)
    expected_3 = {4:1,6:1}
    check(expected_3, output_3)

"""
>>> a
{1: 2, 2: 3}
>>> b = a.keys
>>> b
<built-in method keys of dict object at 0x0000013EDAAA2040>
>>> c = a.keys()
>>> c
dict_keys([1, 2])
>>> d = a.values
>>> d
<built-in method values of dict object at 0x0000013EDAAA2040>
>>> e = a.values()
>>>
>>> e
dict_values([2, 3])
"""