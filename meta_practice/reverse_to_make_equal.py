"""
Reverse to Make Equal
Given two arrays A and B of length N, determine if there is a way to make A equal to B by reversing any subarrays from array B any number of times.
Signature
bool areTheyEqual(int[] arr_a, int[] arr_b)
Input
All integers in array are in the range [0, 1,000,000,000].
Output
Return true if B can be made equal to A, return false otherwise.
Example
A = [1, 2, 3, 4]
B = [1, 4, 3, 2]
output = true
After reversing the subarray of B from indices 1 to 3, array B will equal array A.
"""


1. Using collecions.Counter
Time complexity : O(n)
Space complexity : O(n)

def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
    return Counter(arr) == Counter(target)


1. Sorting the arrays and compare
Time complexity : O(nlogn)
Space complexity : O(1)

def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
    return sorted(arr) == sorted(target)


class Solution:
    def canBeEqual(self, target: List[int], arr: List[int]) -> bool:

        count = [0] * 1001

        for i in range(len(arr)):
            count[arr[i]] += 1
            count[target[i]] -= 1

        return not any(count)


class Solution(object):
    def canBeEqual(self, target, arr):
        """
        :type target: List[int]
        :type arr: List[int]
        :rtype: bool
        """
        def reverse(lst, left, right):

            lst[left:right+1]= reversed(lst[left:right+1])

            return lst

        left=0
        right=1

        while left <right and left < len(target) and right<len(target):
            i=left

            while i < len(target) and target[i]==arr[i]:
                i+=1

            left=i
            right=left+1
            while right < len(target) and target[left]!=arr[right]:
                right+=1
            if left < len(target) and right< len(target):
                #print(left, right, arr, target)
                arr=reverse(arr, left, right)
                #print(left, right, arr, target)
                if arr==target:
                    break

            left+=1
            right=left+1

        return target==arr
    