Write a function to count the number of times each character appears in a string and rewrite the string in that format.
Eg. "I am back." should become "i1 2a2m1b1c1k1.1"

#all_freq = {}

#for i in test_str:    #COunter logic
#   if i in all_freq:
#       all_freq[i] += 1
#   else:
#       all_freq[i] = 1

#from collections import Counter #res = Counter(test_str)
s = "mississiple"     #counter example
a = Counter(s)
>>> a
Counter({'s': 4, 'i': 3, 'm': 1, 'p': 1, 'l': 1, 'e': 1})


input = "I am back."
m = dict(collections.Counter(input))
output = ''
for ch in input:
    output = output + ch + str(m[ch])
print(output)