"""
  want you to write me a simple spell checking engine.  The query language is a very simple regular expression-like language,
   with one special character: . (the dot character), which means EXACTLY ONE character (it can be any character).
   So, for example, 'c.t' would match 'cat' as the dot matches any character.
    There may be any number of dot characters in the query (or none).  Your spell checker will have to be optimized for speed,
    so you will have to write it in the required way. There would be a one-time setUp() function that does any pre-processing you require,
     and then there will be an isMatch() function that should run as fast as possible, utilizing that pre-processing.
     There are some examples below, feel free to ask for clarification.  Word List:  [cat, bat, rat, drat, dart, drab]
Queries:
cat -> true
c.t -> true
.at -> true
..t -> true
d..t -> true
dr.. -> true
... -> true
.... -> true

..... -> false
h.t -> false
c. -> false
*/

"""
import re

if len(word) not in [len(ele) for ele in a]:
    return False
else:
    word_re = re.compile(word)
    for ele in a:
        if len(re.findall(word_re,ele)) > 0 and len(word) == len(ele):
            return True
return False

"""
The Fastest I could get through was this, let me know if any one has a better way other than regex. 
Logic will be this can be implemented in any data structure: """

def func(wrd,lst):
    if len(wrd) not in [len(x) for x in lst]:
        return False
    elif wrd in lst:
        return True
    else:
        lst1 = [x for x in lst if len(x)==len(wrd)]
        for z in lst1:
            c=0
            for i in range(len(wrd)):
                if wrd[i] != '.' and wrd[i] == z[i]:
                    c=c+1
                if len(wrd)-wrd.count('.') == c:
                    return True
return False

