str="Safa Mulani is a student of Engineering discipline."
sub1="Safa"
sub2="Engineering"
print(str.find(substring1))
print(str.find(substring2))

str="Safa Mulani is a student of Engineering discipline."
sub1="Safa"
sub2="Done"
print(sub1 in str)
print(sub2 in str)

str="Safa Mulani is a student of Engineering discipline."
sub1="Safa"
sub2="student"
sub3="Done"
print(str.count(sub1))
print(str.count(sub2))
print(str.count(sub3))

str = "Safa is a Student."
try :
    result = str.index("Safa")
    print ("Safa is present in the string.")
except :
    print ("Safa is not present in the string.") 