#Python3 program to print all the
# strings that match the # given pattern where every # character in the pattern is # uniquely
# mapped to a character # in the dictionary  # Function to encode # given string
def encodeString(Str):

    map = {}
    res = ""
    i = 0

    # For each character     # in given string
    for ch in Str:

        # If the character is occurring         # for the first time, assign next         # unique number to that char
        if ch not in map:
            map[ch] = i
            i += 1

        # Append the number associated         # with current character into         # the output string
        res += str(map[ch])
    return res

# Function to print all # the strings that match the # given pattern where every # character in the pattern is # uniquely mapped to a character
# in the dictionary

def findMatchedWords(dict, pattern):

    # len is length of the     # pattern
    Len = len(pattern)

    # Encode the string
    hash = encodeString(pattern)

    # For each word in the     # dictionary array
    for word in dict:

        # If size of pattern is same           # as size of current         # dictionary word and both         # pattern and the word         # has same hash, print the word
        if(len(word) == Len and
                encodeString(word) == hash):
            print(word, end = " ")

# Driver code
dict = ["abb", "abc","xyz", "xyy" ]
pattern = "foo"
print(encodeString(pattern))
findMatchedWords(dict, pattern)