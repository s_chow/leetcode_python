Given a string queryIP, return "IPv4" if IP is a valid IPv4 address, "IPv6" if IP is a valid IPv6 address or "Neither" if IP is not a correct IP of any type.

A valid IPv4 address is an IP in the form "x1.x2.x3.x4" where 0 <= xi <= 255 and xi cannot contain leading zeros. For example, "192.168.1.1" and "192.168.1.0" are valid IPv4 addresses but "192.168.01.1", while "192.168.1.00" and "192.168@1.1" are invalid IPv4 addresses.

A valid IPv6 address is an IP in the form "x1:x2:x3:x4:x5:x6:x7:x8" where:  1 <= xi.length <= 4 xi is a hexadecimal string which may contain digits, lower-case English letter ('a' to 'f') and upper-case English letters ('A' to 'F'). Leading zeros are allowed in xi. For example, "2001:0db8:85a3:0000:0000:8a2e:0370:7334" and "2001:db8:85a3:0:0:8A2E:0370:7334" are valid IPv6 addresses, while "2001:0db8:85a3::8A2E:037j:7334" and "02001:0db8:85a3:0000:0000:8a2e:0370:7334" are invalid IPv6 addresses.

ip address validation edge case to remember is if there are alphanumeric characters.

class Solution:
    def validIPAddress(self, queryIP: str) -> str:

        def isIp4(string):
            string = string.split(".")
            if len(string) != 4:
                return False
            for part in string:
                if len(part) == 0 or len(part) > 1 and part[0] == '0':
                    return False
                try:
                    if not 0 <= int(part) <= 255:
                        return False
                except ValueError:
                    return False
            return True

        def isIp6(string):
            string = string.split(":")
            if len(string) != 8:
                return False
            for part in string:
                if len(part) > 4:
                    return False
                try:
                    int(part, 16)
                except ValueError:
                    return False
            return True

        if isIp4(queryIP): return "IPv4"
        if isIp6(queryIP): return "IPv6"
        return "Neither"