print("What is your name")
print("concat is done by '+' sigh ")
print('concat is done by "+" sign ')

name = input("Enter your name\n")
#Basically, the difference between raw_input and input is that the return type of raw_input is always string,
# while the return type of input need not be string only. Python will judge as to what data type will it fit the bes

print(type(name)) #for type conversion use str()
#variable names are case sensitive. Eg time_until_midnight is different from time_until_Midnight
print(123_456_768)  #123456768

#Mathematical operetaion priority --> PEMDAS (), **, *, /, + , -

round(2.567, 2)

height = 2.3
weight = 60
print("ypur height and weight are " + str(height) + str(weight))
print(f"ypur height and weight are {height} {weight}")


